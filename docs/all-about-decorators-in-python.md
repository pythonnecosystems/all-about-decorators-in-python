# Python의 '데코레이터'에 관한 모든 것

## Python의 데코레이터란?
데코레이터에 대한 간단한 정의는 다음과 같다. 데코레이터 함수는 다른 함수를 인수로 받아 새로운 함수를 반환하는 고차 함수이다. 이 새 함수는 일반적으로 원래 함수의 동작을 확장하거나 수정한다. 데코레이터 함수는 일반적으로 특수 문자 `@`가 앞에 붙으며 항상 데코레이션되는 함수 위에 배치된다.

다음 코드에서는 데코레이터의 몇몇 예와 원래 소스 코드를 변경하는 함수를 확장하는 데 데코레이터를 사용하는 방법을 보인다.

**예 1:**

```python
# defining normal function

def test():
  print("The addition of two odd numbers 3 and 7")
  print(3 + 7)
  print("is always an even number")

test()
```

Output:

```
The addition of two odd numbers 3 and 7
10
is always an even number
```

```python
# defining a decorator function

def sum_decorator(func):
  def inner_deco():
    print("The addition of two odd numbers 3 and 7")
    func()
    print("is always an even number")
  return inner_deco
```

```python
# funtion without using decorator
def odd_add():
  print(3+7)

odd_add()
```

Output:

```
10
```

```python
# using a decorator to extend the functionality of a normal odd addition function

@sum_decorator
def odd_add():
  print(3+7)

odd_add()
```

Output:

```
The addition of two odd numbers 3 and 7
10
is always an even number
```

**예 2:**

```python
#defining new decorator
def deco(func):
  def inner_deco():
    print("The addition odd numbers upto 10 is")
    func()
  return inner_deco

#using decorator on the function extending its functionality
@deco
def odd_add():
  sum = 0
  for i in range(10):
    if i%2 != 0:
      sum += i
  print(sum)

#calling the decorated function  
odd_add()
```

Output:

```
The addition odd numbers upto 10 is
25
```

## OOP에서 데코레이터가 필요한 이유는 무엇인가? 코딩 시 이점은?

1. **코드 재사용성**: 
데코레이터는 개발자가 공통 기능이나 동작을 여러 함수나 메서드에 적용할 수 있도록 함으로써 코드 재사용성을 높인다. 이 방법은 비슷한 동작을 하는 클래스가 여러 개 있을 때 OOP로 코드를 작성할 때 유용하다.

1. **관심사 분리**: 
데코레이터는 개발자가 코드의 서로 다른 관심사나 측면을 분리하는 데 도움이 된다. 예를 들어 로그인, 인증 등을 위한 데코레이터를 사용할 수 있다. 이렇게 각기 다른 작업에 대해 서로 다른 데코레이터를 사용하면 특히 협업 환경에서 작업할 때 코드베이스가 더 깔끔하고 유지 관리가 쉬워진다.

1. **개방형 또는 폐쇄형 원칙**: 
데코레이터는 객체 지향 프로그래밍의 "개방/폐쇄 원칙"을 따르는데, 이는 클래스나 함수는 확장을 위해 열려 있어야 하지만 수정을 위해 닫혀 있어야 한다는 것이다. 이러한 상황에서 데코레이터를 사용하면 소스 코드를 변경하지 않고도 함수나 메서드의 동작을 확장할 수 있다.

1. **손쉬운 유지 관리**: 
데코레이터를 사용하면 코드베이스 전체를 변경하지 않고도 함수나 메서드의 동작을 항상 한 곳에서 업데이트할 수 있으므로 코드 관리와 유지보수가 더 쉬워진다.

1. **단일 책임 원칙 촉진**: 
데코레이터는 함수나 메서드에 추가 기능을 모듈화할 수 있도록 함으로써 각 함수나 메서드가 단일 책임을 갖도록 도와준다.

1. **가독성**: 
데코레이터는 함수나 메서드의 핵심 기능을 보조 기능과 분리하여 코드 가독성을 향상시킬 수 있다.

## 마치며
대체로 Python의 데코레이터는 함수와 메서드의 동작을 확장하고 수정하는 강력한 도구로, 클래스와 객체를 다루는 객체 지향 프로그래밍에서 특히 유용하다.
