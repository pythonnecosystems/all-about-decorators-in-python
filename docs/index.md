# Python의 '데코레이터'에 관한 모든 것 <sup>[1](#footnote_1)</sup>

![](./1_jlfCr_Q-rqINIiopJ_e_Vw.webp)

> *Python에서 객체 지향 프로그래밍을 공부할 때, 우리는 [Python에서의 OOP (Part 2)](https://pythonnecosystems.gitlab.io/oops-in-python)에서 설명한 것처럼 추상화, 캡슐화, 상속 및 다형성의 네 가지 기둥보다 훨씬 더 많은 것을 배우게 된다.*

이 포스팅에서는 Python의 OOP에서 배운 추가 메서드와 함수 중 하나에 대해 설명한다. 바로 데코레이터이다.

Python에서 데코레이터는 다른 함수나 메서드의 동작을 수정하거나 확장하는 데 사용되는 특수한 타입의 함수이다. 데코레이터는 소스 코드를 직접 수정하지 않고도 함수나 메서드에 추가 기능을 더하기 위해 적용하거나 사용하는 경우가 많다.

<a name="footnote_1">1</a>: [All About “Decorators” in Python](https://medium.com/@shahooda637/all-about-decorators-python-e7260adb9ce7)를 편역한 것이다.
